// SPDX-License-Identifier: GPL-2.0
/*
 * Author: Ayush
 * file: hello.c
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/miscdevice.h>
#include <linux/string.h>

#define ASSIGNED_ID "3302dae3be7a"
#define ID_LENGTH 12
#define DEVICE_NODE "eudyptula"

/* called when reading device node from userspace (ex: # cat /dev/eudyptula) */
static ssize_t mc_read(struct file *filp, char __user *buff, size_t count, loff_t *offp)
{
	return simple_read_from_buffer(buff, count, offp, ASSIGNED_ID, ID_LENGTH);
}

/* called when writing to the device node from the userspace (ex: # echo 74jmn930llma > /dev/eudyptula) */
static ssize_t mc_write(struct file *filp, const char __user *buff, size_t count, loff_t *offp)
{
	char input[16] = {0};
	int ret;

	ret = simple_write_to_buffer(input, sizeof(input), offp, buff, count);

	if (ret < 0)
		return ret;
	/* ID_LENGTH + 1 considering extra NULL in the end */
	if (count != (ID_LENGTH + 1) || strncmp(input, ASSIGNED_ID, ID_LENGTH))
		return -EINVAL;
	return count;

}

/* associating the open file to its own set of functions (driver initialization) */
static const struct file_operations mc_fileops = {
	.owner = THIS_MODULE,
	.read = mc_read,
	.write = mc_write
};

/* Dynamic minor number */
static struct miscdevice mc_device = {
	.name = DEVICE_NODE,
	.fops = &mc_fileops,
	.minor = MISC_DYNAMIC_MINOR,
};

static int __init start(void)
{
	/*
	 * loading ...
	 */

	int ret;

	ret = misc_register(&mc_device);
	if (ret)
		pr_debug("Unable to register misc char driver!");
	else
		pr_debug("Misc char driver registered!");

	return ret;
}

static void __exit end(void)
{
	/*
	 * unloading ...
	 */

	misc_deregister(&mc_device);
	pr_debug("Misc char driver deregistered!!");
}

module_init(start);
module_exit(end);

MODULE_DESCRIPTION("misc char device driver");
MODULE_AUTHOR("Ayush 3302dae3be7a");
MODULE_LICENSE("GPL");
