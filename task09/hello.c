// SPDX-License-Identifier: GPL-2.0
/*
 * Author: Ayush
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/jiffies.h>
#include <linux/slab.h>
#include <linux/mutex.h>
#include <linux/sysfs.h>
#include <linux/kobject.h>
#include <linux/string.h>

#define ASSIGNED_ID "3302dae3be7a"
#define ID_LENGTH 12
#define DEVICE_NODE_DIR "eudyptula"


static DEFINE_MUTEX(key);
static char foo_input[PAGE_SIZE];
static struct kobject *sfsdir;

/* show method for id */
static ssize_t id_show(struct kobject *kobj, struct kobj_attribute *attr,
			char *buf)
{
	return sysfs_emit(buf, "%s\n", ASSIGNED_ID);
}

/* store method for id */
static ssize_t id_store(struct kobject *kobj, struct kobj_attribute *attr,
			const char *buf, size_t count)
{

	/* ID_LENGTH + 1 considering extra NULL in the end */
	if (count != (ID_LENGTH + 1) || strncmp(buf, ASSIGNED_ID, ID_LENGTH))
		return -EINVAL;

	return count;
}


/* show method for foo */
static ssize_t foo_show(struct kobject *kobj, struct kobj_attribute *attr,
		char *buf)
{
	ssize_t result;

	mutex_lock(&key);
	result = sysfs_emit(buf, "%s\n", foo_input);
	mutex_unlock(&key);

	return result;
}

/* store method for foo */
static ssize_t foo_store(struct kobject *kobj, struct kobj_attribute *attr,
				const char *buf, size_t count)
{
	if (count >= PAGE_SIZE)
		return -EINVAL;

	mutex_lock(&key);
	strncpy(foo_input, buf, count);
	mutex_unlock(&key);

	return count;
}

/* show method for jiffies (jiffies is read only) */
static ssize_t jiffies_show(struct kobject *kobj, struct kobj_attribute *attr,
					    char *buf)
{
	return sysfs_emit(buf, "%ld\n", jiffies);
}

/* kobj attribute creation for id, foo & jiffies */
static struct kobj_attribute id_attr = __ATTR_RW(id);
static struct kobj_attribute foo_attr = __ATTR_RW(foo);
static struct kobj_attribute jiffies_attr = __ATTR_RO(jiffies);

static struct attribute *attrs[] = {
	&id_attr.attr,
	&foo_attr.attr,
	&jiffies_attr.attr,
	NULL
};

static struct attribute_group agroup = {
	.attrs = attrs,
};


static int __init start(void)
{
	/*
	 * loading ...
	 */
	int result;
	/* create "eudyptula" dir inside /sys/kernel */
	sfsdir = kobject_create_and_add(DEVICE_NODE_DIR, kernel_kobj);
	if (IS_ERR(sfsdir))
		return -ENOMEM;

	result = sysfs_create_group(sfsdir, &agroup);
	if (result)
		kobject_put(sfsdir);
	pr_debug("init directory!");

	return result;
}

static void __exit end(void)
{
	/*
	 * unloading ...
	 */

	kobject_put(sfsdir);
	pr_debug("directory cleaned!!");
}

module_init(start);
module_exit(end);

MODULE_DESCRIPTION("sysfs: id, jiffies and foo");
MODULE_AUTHOR("Ayush 3302dae3be7a");
MODULE_LICENSE("GPL v2");
