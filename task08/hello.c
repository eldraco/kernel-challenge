// SPDX-License-Identifier: GPL-2.0
/*
 * Author: Ayush
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/debugfs.h>
#include <linux/jiffies.h>
#include <linux/slab.h>
#include <linux/mutex.h>

#define ASSIGNED_ID "3302dae3be7a"
#define ID_LENGTH 12
#define DEVICE_NODE "eudyptula"


static struct dentry *dfsdir;
static DEFINE_MUTEX(key);
static char foo_input[PAGE_SIZE];


/* read method for id */
static ssize_t read_id(struct file *filp, char __user *buff, size_t count,
		loff_t *offp)
{
	return simple_read_from_buffer(buff, count, offp, ASSIGNED_ID, ID_LENGTH);
}

/* write method for id */
static ssize_t write_id(struct file *filp, const char __user *buff, size_t count,
		loff_t *offp)
{
	char input[16] = {0};
	int ret;

	ret = simple_write_to_buffer(input, sizeof(input), offp, buff, count);

	if (ret < 0)
		return ret;
	/* ID_LENGTH + 1 considering extra NULL in the end */
	if (count != (ID_LENGTH + 1) || strncmp(input, ASSIGNED_ID, ID_LENGTH))
		return -EINVAL;
	return count;

}

/* associating the open file to its own set of functions (driver initialization) */
static const struct file_operations debugfs_fileops_id = {
	.owner = THIS_MODULE,
	.read = read_id,
	.write = write_id
};

/* read method for foo */
static ssize_t read_foo(struct file *filp, char __user *buff, size_t count,
		loff_t *offp)
{
	ssize_t result;

	mutex_lock(&key);
	result = simple_read_from_buffer(buff, count, offp, foo_input, strlen(foo_input));
	mutex_unlock(&key);

	return result;
}

/* write method for foo */
static ssize_t write_foo(struct file *filp, const char __user *buff, size_t count,
		loff_t *offp)
{
	ssize_t result;

	if (count >= PAGE_SIZE)
		return -EINVAL;

	mutex_lock(&key);
	result = simple_write_to_buffer(foo_input, PAGE_SIZE, offp, buff, count);
	if (result > 0)
		foo_input[result] = '\0';
	mutex_unlock(&key);

	return result;
}

static const struct file_operations debugfs_fileops_foo = {
	.owner = THIS_MODULE,
	.read = read_foo,
	.write = write_foo
};

static int __init start(void)
{
	/*
	 * loading ...
	 */

	/* NULL means it will create "eudyptula" in /sys/kernel/debug/ */
	dfsdir = debugfs_create_dir("eudyptula", NULL);
	if (dfsdir) {
		debugfs_create_file("id", 0666, dfsdir, NULL, &debugfs_fileops_id);
		pr_debug("/sys/kernel/debug/eudyptula/id created successfully!!");
		debugfs_create_u32("jiffies", 0444, dfsdir, (u32 *)&jiffies);
		pr_debug("/sys/kernel/debug/eudyptula/jiffies created successfully!!");
		debugfs_create_file("foo", 0644, dfsdir, NULL, &debugfs_fileops_foo);
		pr_debug("/sys/kernel/debug/eudyptula/foo created successfully!!");
	} else {
		pr_debug("can't create device!");
		return -ENODEV;
	}
	return 0;
}

static void __exit end(void)
{
	/*
	 * unloading ...
	 */

	debugfs_remove_recursive(dfsdir);
	pr_debug("directory cleaned recursively!!");
}

module_init(start);
module_exit(end);

MODULE_DESCRIPTION("debugfs: id, jiffies and foo");
MODULE_AUTHOR("Ayush 3302dae3be7a");
MODULE_LICENSE("GPL");
