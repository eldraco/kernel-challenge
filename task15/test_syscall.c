#include <unistd.h>
#include <stdio.h>

#define MY_SYSCALL_NR 443

int main(){
        
        unsigned int hi;
        unsigned int lo;

	hi = 0x3302;
	lo = 0xdae3be7a;
	printf("calling with high = 0x%lx and low = 0x%lx returns: ", hi, lo);
	printf("%d\n", syscall(MY_SYSCALL_NR, hi, lo));

	hi = 0x9adf;
	lo = 0xabcdef69;
	printf("calling with high = 0x%lx and low = 0x%lx returns: ", hi, lo);
	printf("%d\n", syscall(MY_SYSCALL_NR, hi, lo));
	return 0;
}

