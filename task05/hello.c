// SPDX-License-Identifier: GPL-2.0
/*
 * Author: Ayush
 * file: hello.c
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/usb.h>
#include <linux/hid.h>

/* table of devices that work with this driver */
static const struct usb_device_id usbkbd_id_table[] = {
	{ 
		USB_INTERFACE_INFO(
				USB_INTERFACE_CLASS_HID,
				USB_INTERFACE_SUBCLASS_BOOT,
				USB_INTERFACE_PROTOCOL_KEYBOARD) 
	},
			{ }
};

/* 
 * used to figure out what devices this driver can control.
 * At compile time, a device table is prepared,
 * when we insert our USB keyboard, then kernel refers to the
 * device table and if an entry is found matching device/vendor
 * id of inserted USB keyboard then its module is loaded and initialized.
 */
MODULE_DEVICE_TABLE (usb, usbkbd_id_table);

static int __init kbd_plugged(void)
{
	/*
	 * loading (on plugging the USB keyboard)...
	 */
	pr_debug("USB Keyboard plugged in!");
	return 0;
}

static void __exit kbd_exit(void)
{
	/*
	 * unloading...
	 */
	pr_debug("Bye world o/");
}

module_init(kbd_plugged);
module_exit(kbd_exit);

MODULE_DESCRIPTION("USB Keyboard autoload module");
MODULE_AUTHOR("Ayush 3302dae3be7a");
MODULE_LICENSE("GPL");
