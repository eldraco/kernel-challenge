// SPDX-License-Identifier: GPL-2.0
/*
 * Author: Ayush
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>


#define FAT_IOCTL_SET_VOLUME_LABEL _IOW('r', 0x14, char *)

int main(void)
{
	char *mount_pt_fat16 = "/mnt/f16";
	char *mount_pt_fat32 = "/mnt/f32";
	char *label_f16 = "newfat16";
	char *label_f32 = "newfat32";

	int fd1 = open(mount_pt_fat16, 0);
	int fd2 = open(mount_pt_fat32, 0);

	if (fd1 < 0 || fd2 < 0)
		return -1;

	if (ioctl(fd1, FAT_IOCTL_SET_VOLUME_LABEL, label_f16)) {
		printf("Failed to set label for FAT16\n");
		return -1;
	}
	if (ioctl(fd2, FAT_IOCTL_SET_VOLUME_LABEL, label_f32)) {
		printf("Failed to set label for FAT32\n");
		return -1;
	}

	printf("label for both devices set successfully\n");
	return 0;
}
