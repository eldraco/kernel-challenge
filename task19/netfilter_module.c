// SPDX-License-Identifier: GPL-2.0
/*
 * Author: Ayush
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/textsearch.h>

#define ASSIGNED_ID "3302dae3be7a"
#define ID_LENGTH 12

static struct nf_hook_ops eudyptula_hook_ops;
static struct ts_config *conf;

static unsigned int hook_method(void *priv, struct sk_buff *skb,
				const struct nf_hook_state *state)
{
	unsigned int pos_id;

	pos_id = skb_find_text(skb, 0, skb->len, conf);
	if (pos_id != UINT_MAX)
		pr_debug("OMG! look what I found at position: %d [hint: it's %s]\n", pos_id, ASSIGNED_ID);

	return NF_ACCEPT;
}

static int __init start(void)
{
	/*
	 * loading ...
	 */
	eudyptula_hook_ops.hook = hook_method;
	eudyptula_hook_ops.hooknum = NF_INET_LOCAL_IN;
	eudyptula_hook_ops.pf = NFPROTO_IPV4;
	eudyptula_hook_ops.priority = NF_IP_PRI_FIRST;

	conf = textsearch_prepare("kmp", ASSIGNED_ID, ID_LENGTH,
			GFP_KERNEL, TS_AUTOLOAD);

	if (IS_ERR(conf)) {
		pr_debug("eudyptula: error in textsearch_prepare");
		return PTR_ERR(conf);
	}

	pr_debug("Hello World o/");

	/* init_net from netfilter header */
	return nf_register_net_hook(&init_net, &eudyptula_hook_ops);
}

static void __exit end(void)
{
	/*
	 * unloading ...
	 */

	nf_unregister_net_hook(&init_net, &eudyptula_hook_ops);
	textsearch_destroy(conf);
	pr_debug("Bye World o/");
}

module_init(start);
module_exit(end);

MODULE_DESCRIPTION("Task 19: netfilter kernel module");
MODULE_AUTHOR("Ayush");
MODULE_LICENSE("GPL v2");
