// SPDX-License-Identifier: GPL-2.0
/*
 * Author: Ayush
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/miscdevice.h>
#include <linux/string.h>
#include <linux/kthread.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/delay.h>

#define ASSIGNED_ID "3302dae3be7a"
#define ID_LENGTH 12
#define DEVICE_NODE "eudyptula"
#define THREAD_NAME "eudyptula"
#define LENGTH 20

static struct task_struct *eudyptula_thread;
/* used to keep track of 'id' */
static int ctr;

static DECLARE_WAIT_QUEUE_HEAD(wee_wait);
static DEFINE_MUTEX(id_mutex);
static LIST_HEAD(identity_list);


struct identity {
	char  name[20];
	int   id;
	bool  busy;
	struct list_head id_list;
};

static int identity_create(char *name, int id)
{
	struct identity *newid;

	newid = kmalloc(sizeof(struct identity), GFP_KERNEL);
	if (!newid)
		return -EINVAL;
	strncpy(newid->name, name, LENGTH);
	newid->name[LENGTH - 1] = '\0';
	newid->id = id;
	newid->busy = false;

	list_add(&(newid->id_list), &identity_list);

	return 0;
}

static struct identity *identity_get(void)
{
	struct identity *temp;

	if (list_empty(&identity_list))
		return NULL;

	if (mutex_lock_interruptible(&id_mutex))
		return NULL;

	temp = list_first_entry(&identity_list, struct identity, id_list);
	list_del(&temp->id_list);
	mutex_unlock(&id_mutex);
	return temp;
}

static ssize_t mc_write(struct file *filp, const char __user *buff,
			size_t count, loff_t *offp)
{
	char input[LENGTH] = {0};
	int ret;
	struct identity *temp;

	ret = simple_write_to_buffer(input, sizeof(input), offp, buff, count);

	if (ret < 0)
		return ret;
	/* Index 19, 0-18 = 19 chars*/
	input[LENGTH - 1] = '\0';

	ret = identity_create(input, ctr);
	if (ret)
		return -EINVAL;

	temp = list_first_entry(&identity_list, struct identity, id_list);

	ctr++;

	wake_up(&wee_wait);

	return count;

}

static const struct file_operations mc_fileops = {
	.owner = THIS_MODULE,
	.write = mc_write
};

/* Dynamic minor number */
static struct miscdevice mc_device = {
	.name  =	DEVICE_NODE,
	.fops  =	&mc_fileops,
	.minor =	MISC_DYNAMIC_MINOR,
	.mode  =	0222,
};

static int thread_function(void *data)
{
	struct identity *temp;

	while (!kthread_should_stop()) {
		/*
		 * only execte when the list isn't empty.
		 */
		wait_event(wee_wait,
			kthread_should_stop() || !list_empty(&identity_list));

		temp = identity_get();

		if (temp) {
			msleep_interruptible(5000);
			pr_debug("Name: %s", temp->name);
			pr_debug("ID: %d", temp->id);
			kfree(temp);
		}

	}
	return 0;
}

static int __init start(void)
{
	/*
	 * loading ...
	 */

	int ret;

	eudyptula_thread = kthread_run(&thread_function, NULL, THREAD_NAME);

	if (IS_ERR(eudyptula_thread))
		pr_debug("kthread creation failed!");
	else
		pr_debug("kthread created succesfully!");

	ret = misc_register(&mc_device);
	if (ret)
		pr_debug("Unable to register misc char driver!");
	else
		pr_debug("Misc char driver registered!");

	return ret;
}

static void __exit end(void)
{
	/*
	 * unloading ...
	 */
	struct identity *t, *n;

	kthread_stop(eudyptula_thread);
	misc_deregister(&mc_device);
	list_for_each_entry_safe(t, n, &identity_list, id_list) {
		list_del(&t->id_list);
		kfree(t);
	}
	pr_debug("Misc char driver deregistered!!");
}

module_init(start);
module_exit(end);

MODULE_DESCRIPTION("Task 18: kthread, wait queue and lists..");
MODULE_AUTHOR("Ayush 3302dae3be7a");
MODULE_LICENSE("GPL v2");
