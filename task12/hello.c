// SPDX-License-Identifier: GPL-2.0
/*
 * Author: Ayush
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/list.h>
#include <linux/slab.h>

#define LENGTH 20

struct identity {
	char  name[20];
	int   id;
	bool  busy;
	struct list_head id_list;
};

static LIST_HEAD(identity_list);

int identity_create(char *name, int id)
{
	struct identity *newid;

	newid = kmalloc(sizeof(struct identity), GFP_KERNEL);
	if (!newid)
		return -EINVAL;
	strncpy(newid->name, name, LENGTH);
	newid->name[LENGTH - 1] = '\0';
	newid->id = id;
	newid->busy = false;
	list_add(&(newid->id_list), &identity_list);

	return 0;
}

struct identity *identity_find(int id)
{
	struct identity *temp = NULL;

	list_for_each_entry(temp, &identity_list, id_list) {
		if (temp->id == id)
			return temp;
	}
	return NULL;


}

void identity_destroy(int id)
{
	struct identity *temp = identity_find(id);

	if (temp) {
		pr_debug("Destroying identity no. %d", temp->id);
		list_del(&(temp->id_list));
		kfree(temp);
	}
}

static int __init start(void)
{
	struct identity *temp;
	int result = 0;

	pr_debug("Hello World o/");
	result = identity_create("Alice", 1);
	if (result)
		return result;

	result = identity_create("Bob", 2);
	if (result)
		return result;

	result = identity_create("Dave", 3);
	if (result)
		return result;

	result = identity_create("Gena", 10);
	if (result)
		return result;

	temp = identity_find(3);
	pr_debug("id 3 = %s\n", temp->name);

	temp = identity_find(42);
	if (temp == NULL)
		pr_debug("id 42 not found\n");

	identity_destroy(2);
	identity_destroy(1);
	identity_destroy(10);
	identity_destroy(42);
	identity_destroy(3);

	return 0;
}

static void __exit end(void)
{
	pr_debug("Bye World o/");
}

module_init(start);
module_exit(end);

MODULE_DESCRIPTION("Task 12: Lists");
MODULE_AUTHOR("Ayush 3302dae3be7a");
MODULE_LICENSE("GPL v2");
