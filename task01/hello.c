/*
 * Author: Ayush
 * file: hello.c
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>

static int __init start(void){
	/*
	 * loading ...
	 */
	pr_debug("Hello World o/");
	return 0;
}

static void __exit end(void){
	/* 
	 * unloading ... 
	 */
	pr_debug("Bye World o/");
}

module_init(start);
module_exit(end);

MODULE_DESCRIPTION("My first kernel module :)");
MODULE_AUTHOR("Ayush");
MODULE_LICENSE("GPL");
